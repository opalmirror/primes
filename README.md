**NAME**

primes - list the first n prime numbers

**SYNOPSIS**

primes *n*

**DESCRIPTION**

List the first *n* prime numbers.

**AUTHOR**

Written by James Perkins, james@loowit.net, 2019.

**COPYING**

Public domain.

**COMPILE WITH**

`g++ -Wall -Wextra -O4 -Wl,--strip-all -o primes primes.cpp`