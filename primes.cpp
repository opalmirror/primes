// NAME
//    primes - list the first n prime numbers
//
// SYNOPSIS
//    primes <n>
//
// DESCRIPTION
//    List the first <n> prime numbers.
//
// AUTHOR
//    Written by James Perkins, james@loowit.net, 2019.
//
// COPYING
//    Public domain.
//
// COMPILE WITH
//    g++ -Wall -Wextra -O4 -Wl,--strip-all -o primes primes.cpp

#include <iostream>

const bool debug = false;
const char *progname = "primes";

// Print a usage message.
void usage(const char *s)
{
    if (s != NULL)
    {
        std::cerr << progname << ": " << s << "\n";
    }

    std::cerr << "usage: " << progname << " <count>\n";

    exit(EXIT_FAILURE);
}

// Parse command line arguments, pass back results.
unsigned int parse(int ac, char **av)
{
    long int count = 0;

    progname = av[0];

    if (ac != 2)
    {
        usage("wrong number of arguments");
    }

    count = strtol(av[1], NULL, 0);

    if (count < 0)
    {
        usage("bad argument");
    }

    if (count < 1)
    {
        exit(EXIT_SUCCESS);
    }

    return count;
}

void primes(unsigned int count)
{
    unsigned int primes[count]; // Array of prime numbers found.
    unsigned int found = 0;     // Number of prime numbers found.
    unsigned int sqr = 0;       // Integer square root of number being tested.
    const unsigned int lowest_prime = 2;

    // Increment n until we've printed a count worth of prime numbers.
    for (unsigned int n = lowest_prime; found < count; n++)
    {
        unsigned int i;

        // sqr gets the square root of n rounded down to nearest integer.
        // This method is faster than sqr = sqrt(n).
        while ((sqr + 1) * (sqr + 1) < n)
        {
            sqr++;
        }

        // Test each prime to see if n is evenly divided.
        for (i = 0; i < found; i++)
        {
            if (n % primes[i] == 0)
            {
                if (debug)
                {
                    std::cerr << "\n" << n << " prime " << primes[i] <<
                        " is a divisor";
                }
                break;
            }

            // Give up immediately if this prime exceeds the integer square root of n.
            if (primes[i] > sqr)
            {
                if (debug)
                {
                    std::cerr << "\n" << n << " prime " << primes[i] <<
                        " is greater than sqrt(n) = " << sqr;
                }
                i = found;
                break;
            }
        }

        // When a new prime is found, remember and print it.
        if (i >= found)
        {
            primes[found++] = n;
            const char *sep = found > 1 ? " " : "";
            std::cout << sep << n;
        }
    }

    std::cout << "\n";
}

int main(int ac, char **av)
{
    unsigned int count = parse(ac, av);
    primes(count);

    exit(EXIT_SUCCESS);
}
