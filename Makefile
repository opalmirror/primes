SRCS := primes.cpp
OBJS := $(SRCS:.cpp=.o)
PROG := primes

CXXFLAGS := -Wall -Wextra -O4
LDFLAGS := -Wl,--strip-all

$(PROG): $(OBJS)
	$(CXX) $(LDFLAGS) -o $@ $(OBJS) $(LOADLIBES) $(LDLIBS)

.PHONY: clean
clean:
	$(RM) $(OBJS) $(PROG)
